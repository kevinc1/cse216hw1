 let rec pow x n =
  if n = 0 then 1
  else if n = 1 then x
  else x * pow x (n-1);; 
  
let rec float_pow x n = 
  if n = 0 then 1.0
  else if n = 1 then x
  else x *. float_pow x (n-1);;

let rec compress list = 
  match list with 
  | [] -> []
  | [_] as y -> y
  | h :: ( h2 :: t)-> if h = h2
                      then compress (h2 :: t)
                      else h :: compress (h :: t);;
 
let rec remove_if list funbool = 
  match list with
  | [] -> []
  | h::t -> if funbool h
            then remove_if t funbool
            else h :: remove_if t funbool;;                      

let rec cut_till_i list i =
  match list with
  | [] -> []
  | h::t -> if i = 0 then h::t else cut_till_i t (i-1);;

let rec add_till_j list j = 
  match list with
  | [] -> []
  | h::t -> if j = 0 then [] else h:: add_till_j t (j-1);;

let slice list i j =
  if i > j then [] else
  let j = j - i in
  add_till_j (cut_till_i list i) j;;

let rec current_equivs ebool list head = 
  match list with
  | [] -> []
  | h::t -> if ebool head h
            then h:: current_equivs ebool t head
            else current_equivs ebool t head;;     

let rec equivs ebool list =
  match list with 
  | [] -> []
  | h::t -> current_equivs ebool list h :: equivs ebool t;;


let check_prime x = 
  if x=0 || x=1 then false else
  let rec not_divisor divisor = 
    if divisor = 0 then true else
    if divisor = 1 then true else
    (x mod divisor != 0) && not_divisor (divisor - 1) in 
  not_divisor (x/2);;

let goldbachpair int = 
  let rec find_pairs num = 
    if check_prime num && check_prime (int - num) then (num, int - num) 
    else find_pairs (num + 1)
  in find_pairs 2;;

let rec equiv_on f g lst = 
  match lst with 
  | [] -> true
  | h::t -> if f h = g h then true && equiv_on f g t else false;;

let rec pairwisefilter cmp lst =
  match lst with
  | [] -> []
  | [_] as default -> default
  | h:: (h2 ::t) -> cmp h h2 :: pairwisefilter cmp t;;
  
let rec polyhelp lst x = 
  match lst with 
  | [] -> 0
  | h::t -> fst h * pow (x) (snd h) + polyhelp t x;;

let polynomial lst = 
  fun x -> polyhelp lst x;;

let rec powerset lst =
  match lst with
  | [] -> []
  | h::t -> [h] :: powerset t;;

let rec powerhelp1 lst head =
  match lst with 
  | [] -> []
  | h::t -> [head; h] :: powerhelp1 t head;;

let rec powerhelp2 lst num =
  if num = 2 then powerhelp2 lst (num-1) else
  if num = 0 then []
  else slice lst 0 num :: powerhelp2 lst (num - 1);;

let rec powerset lst =
  match lst with 
  | [] -> [[]]
  | h::t -> powerhelp1 t h
            @ List.rev (powerhelp2 lst (List.length lst)) 
            @ powerset t;;

 
  