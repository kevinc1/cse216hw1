type bool_expr =
  | Lit of string
  | Not of bool_expr
  | And of bool_expr * bool_expr
  | Or of bool_expr * bool_expr;;

type truth_val = {literal : string; value : bool}
      
let rec truth_helper a b expr = 
  match expr with
  | Lit x ->  if x = a.literal then a.value else b.value
  | Not exp -> not (truth_helper a b exp)
  | And (x,y) -> truth_helper a b x && truth_helper a b y
  | Or (c,d) -> truth_helper a b c || truth_helper a b d;;

let rec truth_table a b expr =
  [(true, true, truth_helper {literal=a; value=true} {literal=b; value=true} expr);
  (true, false, truth_helper {literal=a; value=true} {literal=b; value=false} expr);
  (false, true, truth_helper {literal=a; value=false} {literal=b; value=true} expr);
  (false, false, truth_helper {literal=a; value=false} {literal=b; value=false} expr)];;