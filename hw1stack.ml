type 'a stack = {elements: 'a list};;

let start funct=
  funct {elements = []};;

let push n x = 
  {elements=n::x.elements};;

let rec pop x =
  match x.elements with 
  | [] -> {elements=[]}
  | [_] -> {elements=[]}
  | h::t -> pop {elements=t};;

let halt x =
  x.elements;;



